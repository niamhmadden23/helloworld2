public class HelloWorld {
    public static void main(String[] args) {
        String firstName = "Niamh";
        String surname = "Madden";

        String fullname = getFullName(firstName, surname);
        welcome(fullname);
        ifStatementCode();
        forLoop();
        System.out.println("End of program");
    }

    public static void printLine(){
        System.out.println("##############");
    }

    public static String getFullName (String firstName, String surname){
        return firstName + " " + surname;
    }

    public static void forLoop() {
        String[] animals = {"cat", "dog", "tortoise", "rabbit"};

        int numDogs = 0;
        int numCats = 0;

        for (int i = 0; i < animals.length; i++) {
            if (animals[i] == "cat") {
                numCats++;
            } else if (animals[i] == "dog") {
                numDogs++;
            }
        }
        System.out.println("Number of cats: " + numCats);
        System.out.println("Number of dogs: " + numDogs);
    }

    public static void ifStatementCode() {
        int x = 100;
        if (x < 10) {
            System.out.println("x is less than 10");
        } else if (x <= 100) {
            System.out.println("x is between 10 and 100");
        } else {
            System.out.println("x is greater than 10");
        }
    }

    public static void welcome (String name){
        printLine();
        String output = "Hello " + name;
        System.out.println(output);
        printLine();
    }
}

